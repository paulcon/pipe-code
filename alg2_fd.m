function [Z, e, U, C, delta, pi_outs] = alg2_fd(w, W, quad_pts, quad_wts, ...
    quad_evals, fun, h, logflag)

% Set up
if logflag
    logQ = quad_pts;
else
    logQ = log(quad_pts);
end
omega = quad_wts;
Nquad = length(quad_wts);

% Evaluate the function at the quad points.
Q_outs = quad_evals;

% Let's carefully follow the steps of Alg 2.

% (1) Compute evaluations of the dimensionless dependent variable
pi_outs = Q_outs.*exp(-logQ*w);

% (2) Compute evaluations of the logs of the dimensionless groups
gamma_ins = logQ*W;

% (3) Get perturbed points in logq space for finite differences
gamma_p = [gamma_ins(:,1)+h gamma_ins(:,2); gamma_ins(:,1) gamma_ins(:,2)+h];
logQ_p = (W' \ gamma_p')';

% (4) For each finite difference point, run an experiment. By the way we
% chose the perturbed points, the first half of this vector contains
% perturbations in gamma_1 and the second half contains perturbations in
% gamma_2.
Q_outs_p = fun(logQ_p);

% (5) Compute the corresponding dimensionless dependent variables
pi_outs_p = Q_outs_p.*exp(-logQ_p*w);

% (6) Compute finite differences
dg = [pi_outs_p(1:Nquad)-pi_outs pi_outs_p(Nquad+1:end)-pi_outs] / h;
ndg = sqrt( sum(dg.^2, 2) );

% (7) Estimate active subspaces
G = bsxfun(@times, dg, sqrt(omega)); %./ndg);
C = G'*G;

% Compute eigenvalue decomposition and sort
[~, Sig, U] = svd(G, 'econ');
e = diag(Sig).^2;
fprintf('e1: %6.4e, e2: %6.4e\n', e(1), e(2));

% (8) Compute the weight vectors that define the unique and relevant 
% dimensionless groups.
Z = W*U;

% Post processing to get the ordered dimensionless groups.
delta = logQ*Z;



