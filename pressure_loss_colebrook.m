function dpdx = pressure_loss_colebrook(logQ)

% INPUTS
% rho     = x(1);
% mu      = x(2);
% D       = x(3);
% epsilon = x(4);
% U       = x(5);
%
% OUTPUT
% dpdx

% Set up variables
X = exp(logQ);
rho = X(:,1); mu = X(:,2); D = X(:,3); epsilon = X(:,4); U = X(:,5);

% Compute the Reynolds number
Re = rho.*U.*D./mu;

% Compute coefficients of function to solve with Newton
S = epsilon ./ (3.7 * D);
T = 2.51 ./ Re;

% Newton function and derivative
fun = @(x) x + 2*log10( S + T.*x );
dfun = @(x) 1 + 2*( T ./ ( log(10) * (S + T.*x) ) );

% Using Churchill's explicit formula for Newton initial guess
B = ( 37530 ./ Re ).^(16);
A0 = ( 7./Re ).^(0.9) + 0.27*( epsilon./D );
A1 = 1 ./ A0;
A = ( 2.457*log( A1 ) ).^(16);
lambda = 2*( ( 8 ./ Re ).^(12) + (A + B).^(-1.5) ).^(1/12);
x0 = 1 ./ sqrt(lambda);

% Run Newton
d = 1e9; tol = 1e-9; nx0 = norm(x0);
count = 1;
while d > tol
    
    x1 = x0 - fun(x0) ./ dfun(x0);
    d = norm(x1 - x0);
    
    % Uncomment for diagnostics on Newton iteration
    fprintf('\tIter %d: step size = %6.4e\n', count, d);
    
    x0 = x1;
    count = count + 1;
    
end

% Get friction factor from Newton solution
f = 1 ./ (x0.^2);

% Get pressure drop from friction factor
dpdx = 2*f.*rho.*U.*U./D;




