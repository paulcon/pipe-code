%%
% Script for running algorithms to get relevant and unique dimensionless
% groups. 
clear all; close all;
rng(55);

% Choose parameter regime (1:laminar, 2:turbulent, 3:high Re)
param_case = 2;
[q_l, q_u, caselabel, logcase, logflag] = set_param_case(param_case);

%%
% First, do dimensional analysis.
%
% INPUTS
% rho     = x(1), [1 -3 0]
% mu      = x(2), [1 -1 -1]
% D       = x(3), [0 1 0]
% epsilon = x(4), [0 1 0]
% U       = x(5), [0 1 -1]
%
% BY 
%
% M (kg), L (m), T (s)
%
% OUTPUTS
% dpdx, [1 -2 -2]

D = [1 -3 0; 1 -1 -1; 0 1 0; 0 1 0; 0 1 -1]';
vq = [1 -2 -2]';

% get the rational basis for comparison
W_null = null(D, 'r');

%%
% Set up
nquad = 11;
fun = @(X) pressure_loss_colebrook(X);
[w, W, Q_ins, omega, Q_outs] = alg2_fd_setup(D, vq, nquad, fun, q_l, q_u, logflag);

%%
% Run Algorithm 2
h = 1e-6;
[Z, e, U, C, delta, pi_outs] = alg2_fd(w, W, Q_ins, omega, Q_outs, fun, h, logflag);

%%
% Print out Z
Z

%[numZ, demonZ] = rat(Z, 1e-3)

% Let's make a 2-d summary plot 
indz = randi(size(pi_outs,1), 1, 5000);

% With rotate variables
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3)=5; pp(4)=5;
set(gcf, 'PaperPosition', pp);
scatter(-delta(indz, 1), delta(indz, 2), 80, log(pi_outs(indz)), 'filled'); 
axis square; grid on;
set(gca, 'FontSize', 16);
xlabel('$\log(\hat{\pi}_1)$', 'interpreter', 'latex'); 
ylabel('$\log(\hat{\pi}_2)$', 'interpreter', 'latex');
%title(sprintf('%s, %s', caselabel, logcase));
colorbar;
print(sprintf('figs/ssp_alg2_%s', caselabel), '-depsc2');

% With standard dimensionless groups (Re, epsilon/D)
delta_da = log(Q_ins)*fliplr(W_null);

figure(2);
pp = get(gcf, 'PaperPosition');
pp(3)=5; pp(4)=5;
set(gcf, 'PaperPosition', pp);
scatter(delta_da(indz, 1), delta_da(indz, 2), 80, log(pi_outs(indz)), 'filled'); 
axis square; grid on;
set(gca, 'FontSize', 16);
xlabel('$\log(\pi_1)$', 'interpreter', 'latex'); 
ylabel('$\log(\pi_2)$', 'interpreter', 'latex');
%title(sprintf('%s, %s', caselabel, logcase));
colorbar;
print(sprintf('figs/ssp_da_%s', caselabel), '-depsc2');

fprintf('FD rotates DA by %6.4f degrees\n', acos(U(1,1))*(180/pi));

%%
% Express the new z variables in terms of the classical Reynolds number and
% roughness scale.
Re = [1; -1; 1; 0; 1]; ruf = [0; 0; -1; 1; 0];
W = [Re ruf];

c = W \ Z(:,1);
fprintf('z_1 in terms of Re and roughness: %6.4f, %6.4f\n', c(1), c(2));
fprintf('Resid: %6.4e\n', norm(W*c - Z(:,1)) );

c = W \ Z(:,2);
fprintf('z_2 in terms of Re and roughness: %6.4f, %6.4f\n', c(1), c(2));
fprintf('Resid: %6.4e\n', norm(W*c - Z(:,2)) );


