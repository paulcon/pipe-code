function [q_l, q_u, caselabel, logcase, logflag] = set_param_case(param_case)

% Set bounds
switch param_case
    % rho     = x(1);
    % mu      = x(2);
    % D       = x(3);
    % epsilon = x(4);
    % U       = x(5);
    case 1
        % Laminar Region
        q_l = [1.0e-1 1e-6 5e-1 3e-5 2.5e-2]; % lower parameter bound
        q_u = [1.4e-1 1e-5 8e-1 8e-5 3.0e-2]; % upper parameter bound
        fprintf('==== LAMINAR ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'LAMINAR';
    case 2
        % Turbulent Region
        q_l = [1.0e-1 1e-6 5e-1 5e-4 2.0e+0]; % lower parameter bound
        q_u = [1.4e-1 1e-5 1e+0 2e-3 4.0e+0]; % upper parameter bound
        fprintf('==== TURBULENT ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'TURBULENT';
    case 3
        % Re independent
        q_l = [1.0e-1 1e-6 5e-1 1e-2 5.0e+2]; % lower parameter bound
        q_u = [1.4e-1 1e-5 1e+0 4e-2 7.0e+2]; % upper parameter bound
        fprintf('==== HIGHRE ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'HIGHRE';
    otherwise
        error('Unrecognized parameter case! param_case=%d', param_case);
end

% If logflag, then uniform density on logq. Else, uniform density on q.
logflag = 0;
if logflag 
    logcase = 'log'; 
    fprintf('LOG\n');
else
    fprintf('NOLOG\n');
    logcase = 'nolog';
end

