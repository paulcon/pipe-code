%%
% Checking the ridge function theory.
close all; clear all;

%%
% Choose a parameter regime

% Choose parameter regime (1:laminar, 2:turbulent)
param_case = 3;
[q_l, q_u, caselabel, logcase, logflag] = set_param_case(param_case);

% Get quadrature points.
quadOrder = 11;
m = length(q_l);
s = [];
for i=1:m, s = [s; parameter()]; end
[X, omega] = gaussian_quadrature(s, quadOrder*ones(1,m));
Nquad = size(X, 1);

% Sample from log space
%logq_l = log(q_l); logq_u = log(q_u);
%logQ = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (logq_u-logq_l)), logq_l);

% Sample from uniform space
Q = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (q_u-q_l)), q_l);
logQ = log(Q);

% Get outputs at quad points
Q_out = pressure_loss_colebrook(logQ);

% Compute finite differences
hz = 2.^([-5:-2:-15]);
nhz = length(hz);
errz = zeros(nhz, m);
e0 = zeros(m, 1);

for k = 1:nhz
    h = hz(k);
    dQ = zeros(Nquad, m);
    for i=1:m

        logQ_p = logQ;
        logQ_p(:,i) = logQ_p(:,i) + h;
        Q_out_p = pressure_loss_colebrook(logQ_p);
        dQ(:,i) = ( Q_out_p - Q_out ) / h;

    end
    
    % Compute the active subspaces
    C = dQ'*(bsxfun(@times, dQ, omega));

    % Compute eigenvalue decomposition and sort
    [~, E] = eig(C);
    [e1, ~] = sort(diag(E), 'descend');

    errz(k,:) = abs(e1)'; % - e0(4:5))'; % ./ abs(e0(4:5)))';
    
    e0 = e1;
    
end
    
    
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3)=4; pp(4)=4;
set(gcf, 'PaperPosition', pp);
loglog(hz, errz, 'o-', 'LineWidth', 2);
axis square; grid on; 
set(gca, 'FontSize', 16);
xlabel('$h$', 'interpreter', 'latex'); ylabel('$|\lambda_k(h)|$', 'interpreter', 'latex');
legend({'$\lambda_1$', '$\lambda_2$', '$\lambda_3$', '$\lambda_4$', '$\lambda_5$'}, ...
    'interpreter', 'latex', ...
    'Location', 'SouthEast');
%title(sprintf('%s eigenvalue errors', caselabel));
print(sprintf('figs/unif_eigerr_%s', caselabel), '-depsc2');

% Compute the active subspaces
C = dQ'*(bsxfun(@times, dQ, omega));

% Compute eigenvalue decomposition and sort
[U, E] = eig(C);
[e, ind] = sort(diag(E), 'descend');

fprintf('eigs:\n');
for i=1:m, fprintf('\t%7.6e\n', e(i)); end







