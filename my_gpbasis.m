function H = my_gpbasis(X)

[N, m] = size(X);

I = index_set('full', 2, m);
n = size(I, 2);

H = zeros(N, n);
for i=1:n
   
    ind = I(:,i)';
    H(:,i) = prod(bsxfun(@power, X, ind), 2);
    
end

end