function dpdx = pressure_loss_churchill(logQ)

% INPUTS
% rho     = x(1);
% mu      = x(2);
% D       = x(3);
% epsilon = x(4);
% U       = x(5);
%
% OUTPUT
% dpdx

% Set up variables
X = exp(logQ);
rho = X(:,1); mu = X(:,2); D = X(:,3); epsilon = X(:,4); U = X(:,5);

% Compute the Reynolds number
Re = rho.*U.*D./mu;

% Check if flow is turbulent
% Re_cr = 2000;
% nlam = sum(Re < Re_cr);
% fprintf('Laminar: %d, Turbulent: %d\n', nlam, length(Re) - nlam);

% Using Churchill's explicit formula for friction factor
B = ( 37530 ./ Re ).^(16);
A0 = ( 7./Re ).^(0.9) + 0.27*( epsilon./D );
A1 = 1 ./ A0;
A = ( 2.457*log( A1 ) ).^(16);
f = 2*( ( 8 ./ Re ).^(12) + (A + B).^(-1.5) ).^(1/12);

% Get pressure drop from friction factor
dpdx = f.*rho.*U.*U./D;




