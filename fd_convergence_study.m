%%
% Relative convergence study for finite differences.
% Finds that h = 5e-7 is a good step size.
close all; clear all;
rng(55);

% Choose parameter regime (1:laminar, 2:turbulent, 3: high Re)
param_case = 3;
[q_l, q_u, caselabel, logcase, logflag] = set_param_case(param_case);

%%
% First, do dimensional analysis.
%
% INPUTS
% rho     = x(1), [1 -3 0]
% mu      = x(2), [1 -1 -1]
% D       = x(3), [0 1 0]
% epsilon = x(4), [0 1 0]
% U       = x(5), [0 1 -1]
%
% BY 
%
% M (kg), L (m), T (s)
%
% OUTPUTS
% dpdx, [1 -2 -2]

D = [1 -3 0; 1 -1 -1; 0 1 0; 0 1 0; 0 1 -1]';
vq = [1 -2 -2]';

%%
% set up
nquad = 11;
fun = @(X) pressure_loss_colebrook(X);
[w, W, Q_ins, omega, Q_outs] = alg2_fd_setup(D, vq, nquad, fun, q_l, q_u, logflag);

% h convergence study for Z
hz = 2.^(-10:-2:-26);
nhz = length(hz);
Z_errz = zeros(nhz, 2);
Z0 = eye(size(D,2), size(D,2) - size(D,1));

for i=1:nhz
   
    h = hz(i);
    [Z1, e, U, C, delta, pi_outs] = alg2_fd(w, W, Q_ins, omega, Q_outs, fun, h, logflag);
    
    Z_errz(i,1) = min( norm(Z1(:,1) - Z0(:,1)), norm(Z1(:,1) + Z0(:,1)) );
    Z_errz(i,2) = min( norm(Z1(:,2) - Z0(:,2)), norm(Z1(:,2) + Z0(:,2)) );
    
    Z0 = Z1;
    
end

figure(1);

pp = get(gcf, 'PaperPosition');
pp(3)=4; pp(4)=4;
set(gcf, 'PaperPosition', pp);

loglog(hz(2:end), Z_errz(2:end,1), 'bo--', ...
    hz(2:end), Z_errz(2:end,2), 'rx--', ...
    'LineWidth', 2, 'MarkerSize', 12);
axis square; grid on;
set(gca, 'FontSize', 16);
xlabel('$h$', 'interpreter', 'latex');
ylabel('Relative difference in $\mathbf{z}$', 'interpreter', 'latex');
legend('1', '2', 'Location', 'NorthWest');
xlim([min(hz) max(hz)]); ylim([1e-10, 1e-3]);
print(sprintf('figs/zerr_%s', caselabel), '-depsc2');


