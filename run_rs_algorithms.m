%%
% Script for running algorithms to get relevant and unique dimensionless
% groups. 
close all; clear all;
rng(55);

% Choose parameter regime (1:laminar, 2:turbulent)
param_case = 2;
[q_l, q_u, caselabel, logcase, logflag] = set_param_case(param_case);

%%
% First, do dimensional analysis.
%
% INPUTS
% rho     = x(1), [1 -3 0]
% mu      = x(2), [1 -1 -1]
% D       = x(3), [0 1 0]
% epsilon = x(4), [0 1 0]
% U       = x(5), [0 1 -1]
%
% BY 
%
% M (kg), L (m), T (s)
%
% OUTPUTS
% dpdx, [1 -2 -2]

D = [1 -3 0; 1 -1 -1; 0 1 0; 0 1 0; 0 1 -1]';
vq = [1 -2 -2]';

%%
% Set up
ndesign = 1000;
fun = @(X) pressure_loss_colebrook(X);
[w, W, Q_ins, Q_outs] = alg1_rs_setup(D, vq, ndesign, fun, q_l, q_u, logflag);

%%
% Run Algorithm 1
h = 1e-6;
nquad = 11;
[Z, e, U, C, delta, pi_outs] = alg1_rs(w, W, Q_ins, Q_outs, h, nquad, q_l, q_u, logflag);

%%
% Print out Z
Z

% With rotate variables
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3)=5; pp(4)=5;
set(gcf, 'PaperPosition', pp);
scatter(delta(:, 1), delta(:, 2), 80, log(pi_outs), 'filled'); 
axis square; grid on;
set(gca, 'FontSize', 16);
xlim([0.9*min(delta(:,1)) 1.1*max(delta(:,1))]);
xlabel('$\log(\hat{\pi}_1)$', 'interpreter', 'latex'); 
ylabel('$\log(\hat{\pi}_2)$', 'interpreter', 'latex');
%title(sprintf('%s, %s', caselabel, logcase));
colorbar;
print(sprintf('figs/ssp_alg1_%s', caselabel), '-depsc2');

%%
% Express the new z variables in terms of the classical Reynolds number and
% roughness scale.
Re = [1; -1; 1; 0; 1]; ruf = [0; 0; -1; 1; 0];
W = [Re ruf];

c = W \ Z(:,1);
fprintf('z_1 in terms of Re and roughness: %6.4f, %6.4f\n', c(1), c(2));
fprintf('Resid: %6.4e\n', norm(W*c - Z(:,1)) );

c = W \ Z(:,2);
fprintf('z_2 in terms of Re and roughness: %6.4f, %6.4f\n', c(1), c(2));
fprintf('Resid: %6.4e\n', norm(W*c - Z(:,2)) );



