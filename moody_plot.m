%%
% A script to generate the Moody surface
close all; clear all;

% Parameter ranges
ngrid = 101;
Re = linspace(2, 8, ngrid);
ruf = linspace(-6, -1, ngrid); % ruf is epsilon / D
[X1, X2] = meshgrid(Re, ruf);

% Compute coefficients of function to solve with Newton
S = (10.^X2(:)) ./ 3.7;
T = 2.51 ./ (10.^X1(:));

% Newton function and derivative
fun = @(x) x + 2*log10( S + T.*x );
dfun = @(x) 1 + 2*( T ./ ( log(10) * (S + T.*x) ) );

% Using Churchill's explicit formula for Newton initial guess
B = ( 37530 ./ (10.^X1(:)) ).^(16);
A0 = ( 7./ (10.^X1(:)) ).^(0.9) + 0.27*( (10.^X2(:)) );
A1 = 1 ./ A0;
A = ( 2.457*log( A1 ) ).^(16);
lambda = 2*( ( 8 ./ (10.^X1(:)) ).^(12) + (A + B).^(-1.5) ).^(1/12);
x0 = 1 ./ sqrt(lambda);

% Run Newton
d = 1e9; tol = 1e-9; nx0 = norm(x0);
count = 1;
while d > tol
    
    x1 = x0 - fun(x0) ./ dfun(x0);
    d = norm(x1 - x0);
    
    % Uncomment for diagnostics on Newton iteration
    fprintf('\tIter %d: step size = %6.4e\n', count, d);
    
    x0 = x1;
    count = count + 1;
    
end

% Get friction factor from Newton solution
f = 1 ./ (x0.^2);
F = reshape(f, ngrid, ngrid);

% Surface plots
% figure(1);
% 
% pp = get(gcf, 'PaperPosition');
% pp(3) = 5; pp(4) = 5;
% set(gcf, 'PaperPosition', pp);
% 
% surf(X1, X2, F, 'edgecolor', 'none');
% axis square; grid on; view(2);
% set(gca, 'FontSize', 16);
% xlabel('$\log_{10}(\mbox{Re})$', 'interpreter', 'latex');
% ylabel('$\log_{10}(\mbox{roughness})$', 'interpreter', 'latex');
% colorbar;
% print(sprintf('figs/moody_v2'), '-depsc2');
% 
% figure(2);
% 
% pp = get(gcf, 'PaperPosition');
% pp(3) = 5; pp(4) = 5;
% set(gcf, 'PaperPosition', pp);
% 
% surf(X1, X2, F, 'edgecolor', 'none');
% axis square; grid on;
% set(gca, 'FontSize', 16);
% xlabel('$\log_{10}(\mbox{Re})$', 'interpreter', 'latex');
% ylabel('$\log_{10}(\mbox{roughness})$', 'interpreter', 'latex');
% zlabel('Friction factor', 'interpreter', 'latex');
% zlim([0 0.21]);
% print(sprintf('figs/moody_v1'), '-depsc2');

% Find zonotopes in (Re, ruf) space for each case
Re_vec = [1; -1; 1; 0; 1];
ruf_vec = [0; 0; -1; 1; 0];
A = [Re_vec ruf_vec];
corners = index_set('tensor', ones(1,5));
corners(corners==0) = -1;
corners = corners';

figure(1); hold on;

pp = get(gcf, 'PaperPosition');
pp(3) = 5; pp(4) = 5;
set(gcf, 'PaperPosition', pp);

%surf(X1, X2, F, 'edgecolor', 'none'); view(2);
contour(X1, X2, F, 20, 'linewidth', 2);
axis square; grid on; 
set(gca, 'FontSize', 16);
xlabel('$\log_{10}(\mbox{Re})$', 'interpreter', 'latex');
ylabel('$\log_{10}(\varepsilon / D)$', 'interpreter', 'latex');
cb = colorbar;
cb.Label.String = 'Friction factor';
cb.Label.Interpreter = 'latex';
cb.Label.FontSize = 16;

% LAMINAR
[q_l, q_u, ~, ~, ~] = set_param_case(1);
corners_lam = bsxfun(@plus, bsxfun(@times, 0.5*(corners + 1), (log10(q_u) - log10(q_l))), log10(q_l));
Z = corners_lam*A;
K = convhull(Z(:,1), Z(:,2));
plot3(Z(K,1), Z(K,2), ones(length(K),1), 'r:', 'LineWidth', 4);

% TURBULENT
[q_l, q_u, ~, ~, ~] = set_param_case(2);
corners_tur = bsxfun(@plus, bsxfun(@times, 0.5*(corners + 1), (log10(q_u) - log10(q_l))), log10(q_l));
Z = corners_tur*A;
K = convhull(Z(:,1), Z(:,2));
plot3(Z(K,1), Z(K,2), ones(length(K),1), 'k:', 'LineWidth', 4);

% HIGH RE
[q_l, q_u, ~, ~, ~] = set_param_case(3);
corners_fas = bsxfun(@plus, bsxfun(@times, 0.5*(corners + 1), (log10(q_u) - log10(q_l))), log10(q_l));
Z = corners_fas*A;
K = convhull(Z(:,1), Z(:,2));
plot3(Z(K,1), Z(K,2), ones(length(K),1), 'm:', 'LineWidth', 4);

hold off;

print(sprintf('figs/moody_contour'), '-depsc2');









