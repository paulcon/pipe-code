%%
% Script for running algorithms to get relevant and unique dimensionless
% groups. 
clear all; close all;
rng(55);

% Choose parameter regime (1:laminar, 2:turbulent)
param_case = 2;

% Set bounds
switch param_case
    % rho     = x(1);
    % mu      = x(2);
    % D       = x(3);
    % epsilon = x(4);
    % U       = x(5);
    case 1
        % Laminar Region
        q_l = [1.0e-1 1e-6 1e-1 1e-3 1.0e-4]; % lower parameter bound
        q_u = [1.4e-1 1e-5 1e+0 1e-1 1.0e-3]; % upper parameter bound
        fprintf('==== LAMINAR ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'LAMINAR';
    case 2
        % Turbulent Region
        q_l = [1.0e-1 1e-6 1e-1 1e-3 1.0e+1]; % lower parameter bound
        q_u = [1.4e-1 1e-5 1e+0 1e-1 5.0e+1]; % upper parameter bound
        fprintf('==== TURBULENT ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'TURBULENT';
    case 3
        % Re independent
        q_l = [1.0e-1 1e-6 1e-1 1e-3 1.0e+4]; % lower parameter bound
        q_u = [1.4e-1 1e-5 1e+0 1e-1 5.0e+4]; % upper parameter bound
        fprintf('==== FAST ====\n');
        Re_l = q_l(1)*q_l(3)*q_l(5)/q_u(2);
        Re_u = q_u(1)*q_u(3)*q_u(5)/q_l(2);
        fprintf('Re range: [%4.2e, %4.2e]\n', Re_l, Re_u);
        fprintf('epsilon / D range: [%4.2e, %4.2e]\n', q_l(4)/q_u(3), q_u(4)/q_l(3));
        caselabel = 'FAST';
    otherwise
        error('Unrecognized parameter case! param_case=%d', param_case);
end

% If logflag, then uniform density on logq. Else, uniform density on q.
logflag = 0;
if logflag 
    logcase = 'log'; 
    fprintf('LOG\n');
else
    fprintf('NOLOG\n');
    logcase = 'nolog';
end

%%
% First, do dimensional analysis.
%
% INPUTS
% rho     = x(1), [1 -3 0]
% mu      = x(2), [1 -1 -1]
% D       = x(3), [0 1 0]
% epsilon = x(4), [0 1 0]
% U       = x(5), [0 1 -1]
%
% BY 
%
% M (kg), L (m), T (s)
%
% OUTPUTS
% dpdx, [1 -2 -2]

D = [1 -3 0; 1 -1 -1; 0 1 0; 0 1 0; 0 1 -1]';
vq = [1 -2 -2]';

% get the rational basis for comparison
W_null = null(D, 'r');

%%
% Let's look real quick at the pseudospectral coefficients of pressure loss
% as a function of logq as a diagnostic.

% ps_fun = @(X) pressure_loss(bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (logq_u-logq_l)), logq_l));
% pOrder = 11;
% PS = pseudospectral(ps_fun, s, pOrder);
% 
% figure;
% semilogy(sum(PS.index_set, 1), abs(PS.coefficients), '.');
% axis square; grid on; 
% set(gca, 'FontSize', 14);
% xlabel('Order'); ylabel('abs(Coeff)');

% Looks pretty good! Down to 1e-12.

%%
% Compare Churchill pressure loss to Colebrook with Newton
nquad = 3;

% Churchill
fun = @(X) pressure_loss_churchill(X);
tic;
[w, W, Q_ins, omega, Q_outs_churchill] = alg2_fd_setup(D, vq, nquad, fun, q_l, q_u, logflag);
fprintf('Churchill time: %4.2f\n', toc);

%% Colebrook with Newton
fun = @(X) pressure_loss_colebrook(X);
tic;
[w, W, Q_ins, omega, Q_outs_colebrook] = alg2_fd_setup(D, vq, nquad, fun, q_l, q_u, logflag);
fprintf('Colebrook time: %4.2f\n', toc);

%%
% How different are Churchill and Colebrook?
fprintf('Norm diff Churchill and Colebrook: %6.4e\n', norm(Q_outs_churchill - Q_outs_colebrook));





