function [Z, e, U, C, delta, pi_outs] = alg1_rs(w, W, Q_ins, Q_outs, ...
    h, nquad, q_l, q_u, logflag)

% Set up
if logflag
    logQ = Q_ins;
else
    logQ = log(Q_ins);
end

% Let's carefully follow the steps of Alg 1.

% (1) Compute evaluations of the dimensionless independent variable
pi_outs = Q_outs.*exp(-logQ*w);

% (2) Compute evaluations of the logs of the dimensionless groups
gamma_ins = logQ*W;

% (3) Build a GP
n = size(pi_outs, 2);
nbasis = nchoosek(n+2, 2);
gprMdl = compact(...
    fitrgp(gamma_ins, pi_outs, ...
    'FitMethod', 'exact', ...
    'BasisFunction', @my_gpbasis,...
    'Beta', ones(nbasis, 1), ...
    'KernelFunction', 'ardsquaredexponential', ...
    'PredictMethod', 'exact', 'Verbose', 1));

% (4) Get active subspaces from gradient of response surface
dfun = @(X) gp_finite_differences(gprMdl, h, X);
[e, U, C] = compute_active_subspaces(nquad, W, dfun, q_l, q_u, logflag);

% (8) Compute the weight vectors that define the unique and relevant 
% dimensionless groups.
Z = W*U; 

% Post processing to get the ordered dimensionless groups.
delta = exp(logQ*Z);

end

function [e, U, C] = compute_active_subspaces(nquad, W, dfun, q_l, q_u, logflag)

% Get quadrature rule
m = 5;
s = [];
for i=1:m, s = [s; parameter()]; end
[X, omega] = gaussian_quadrature(s, nquad*ones(1,m));

% Set bounds and draw quadrature points
if logflag
    % case of uniform density on logq
    
    logq_l = log(q_l); logq_u = log(q_u);
    logQ = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (logq_u-logq_l)), logq_l);
    
else
    % case of uniform density on q
    Q_ins = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (q_u-q_l)), q_l);
    logQ = log(Q_ins);
    
end

% Compute the gammas
gamma = logQ*W;

% Evaluate the gradient at the quadrature points
dg = dfun(gamma);

% compute C matrix
G = bsxfun(@times, dg, sqrt(omega));
C = G'*G;

% compute eigenvalue decomposition
[~, Sig, U] = svd(G, 'econ');
e = diag(Sig).^2;
fprintf('e1: %6.4e, e2: %6.4e\n', e(1), e(2));

end

function dQ = gp_finite_differences(gprMdl, h, gamma)

n = size(gamma, 2);

% base point
Q_0 = predict(gprMdl, gamma);
dQ = zeros(size(gamma));
for i=1:n
   
    % perturbation
    gamma_p = gamma;
    gamma_p(:,i) = gamma(:,i) + h;
    Q_p = predict(gprMdl, gamma_p);
    
    % finite difference
    dQ(:,i) = (Q_p - Q_0) / h;
    
end

end