function [w, W, Q_ins, Q_outs] = alg1_rs_setup(D, vq, ndesign, fun, q_l, q_u, logflag)

% Getting a basis for dimensional analysis
W = null(D);

% Make w consistent with the friction factor nondimensionalization
% w = D\vq;
w = [1; 0; -1; 0; 2];
if norm(D*w-vq) > sqrt(eps), error('Bad normalization vector'); end

% Latin hypercube design
m = length(q_l);
X = lhsdesign(ndesign, m);
Q = bsxfun(@plus,bsxfun(@times, X, (q_u-q_l)), q_l);

% Set bounds and draw quadrature points
if logflag
    % case of uniform density on logq
    
    logq_l = log(q_l); logq_u = log(q_u);
    logQ = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (logq_u-logq_l)), logq_l);
    Q_ins = logQ;
    
else
    % case of uniform density on q
    Q_ins = bsxfun(@plus,bsxfun(@times, 0.5*(X+1), (q_u-q_l)), q_l);
    logQ = log(Q_ins);
    
end

% Evaluate the pressure loss.
Q_outs = fun(logQ);